const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const fileUpload = require('express-fileupload')
const favicon = require('serve-favicon')
const cookieParser = require('cookie-parser')
const helmet = require('helmet')
const morgan = require('morgan')

const app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')
app.use(fileUpload())
app.use(helmet.frameguard())
app.use(helmet.xssFilter())
app.use(helmet.noSniff())
app.disable('x-powered-by')
app.use(favicon(path.join(__dirname, 'public', 'icon.png')))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

// Logger
app.use(morgan('dev'))

// Call Router
app.use('/dashboard', require('./routes/dashboard'))
app.use('/setting', require('./routes/setting'))
app.use('/averagePresence', require('./routes/averagePresence'))
app.use('/student', require('./routes/student'))
app.use('/404', require('./routes/404'))

app.get('/', (req, res) => {
  res.render('login/login')
})

app.use((err, res, next) => {
  if (err) {
    res.redirect('/404')
  }
})

app.listen(8000, function () {
  console.log('Server started on http://localhost:8000')
})

module.exports = app
