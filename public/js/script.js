$(document).ready(function () {
  // line one
  $('.btn-present-one').click(function(){
    $('.btn-present-one').css('background-color', 'green')
    $('.btn-present-one').css('color', 'white')
    $('.btn-absent-one').prop('disabled', 'disabled')
    $('.btn-late-one').prop('disabled', 'disabled')
    })
  $('.btn-late-one').click(() => {
    $('.btn-late-one').css('background-color', '#eed202')
    $('.btn-absent-one').prop('disabled', 'disabled')
    $('.btn-present-one').prop('disabled', 'disabled')
  })
  $('.btn-absent-one').click(() => {
    $('.btn-absent-one').css('background-color', 'red')
    $('.one').css('color', 'white')
    $('.btn-absent-one').css('color', 'white')
    $('.btn-late-one').prop('disabled', 'disabled')
    $('.btn-present-one').prop('disabled', 'disabled')
  })
  // Line two
  $('.btn-present-two').click(function(){
    $('.btn-present-two').css('background-color', 'green')
    $('.btn-present-two').css('color', 'white')
    $('.btn-absent-two').prop('disabled', 'disabled')
    $('.btn-late-two').prop('disabled', 'disabled')
  })
  $('.btn-late-two').click(() => {
    $('.btn-late-two').css('background-color', '#eed202')
    $('.btn-absent-two').prop('disabled', 'disabled')
    $('.btn-present-two').prop('disabled', 'disabled')
  })
  $('.btn-absent-two').click(() => {
    $('.btn-absent-two').css('background-color', 'red')
    $('.two').css('color', 'white')
    $('.btn-absent-two').css('color', 'white')
    $('.btn-late-two').prop('disabled', 'disabled')
    $('.btn-present-two').prop('disabled', 'disabled')
  })
  // Line three
  $('.btn-present-three').click(function(){
    $('.btn-present-three').css('background-color', 'green')
    $('.btn-present-three').css('color', 'white')
    $('.btn-absent-three').prop('disabled', 'disabled')
    $('.btn-late-three').prop('disabled', 'disabled')
  })
  $('.btn-late-three').click(() => {
    $('.btn-late-three').css('background-color', '#eed202')
    $('.btn-absent-three').prop('disabled', 'disabled')
    $('.btn-present-three').prop('disabled', 'disabled')
  })
  $('.btn-absent-three').click(() => {
    $('.btn-absent-three').css('background-color', 'red')
    $('.three').css('color', 'white')
    $('.btn-absent-three').css('color', 'white')
    $('.btn-late-three').prop('disabled', 'disabled')
    $('.btn-present-three').prop('disabled', 'disabled')
  })
  // line four
  $('.btn-present-four').click(function(){
    $('.btn-present-four').css('background-color', 'green')
    $('.btn-present-four').css('color', 'white')
    $('.btn-absent-four').prop('disabled', 'disabled')
    $('.btn-late-four').prop('disabled', 'disabled')
  })
  $('.btn-late-four').click(() => {
    $('.btn-late-four').css('background-color', '#eed202')
    $('.btn-absent-four').prop('disabled', 'disabled')
    $('.btn-present-four').prop('disabled', 'disabled')
  })
  $('.btn-absent-four').click(() => {
    $('.btn-absent-four').css('background-color', 'red')
    $('.four').css('color', 'white')
    $('.btn-absent-four').css('color', 'white')
    $('.btn-late-four').prop('disabled', 'disabled')
    $('.btn-present-four').prop('disabled', 'disabled')
  })
})